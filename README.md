I might make an NFL stats site.

Look at this video: http://www.nfl.com/gamecenter/2016091103/2016/REG1/packers@jaguars#menu=drivechart%7CcontentId%3A0ap3000000698230&tab=videos and this JSON feed: http://www.nfl.com/liveupdate/game-center/2016091103/2016091103_gtd.json?random=150021957000

(Note: We can just as easily do CFLativity: https://www.cfl.ca/wp-content/themes/cfl.ca/inc/admin-ajax.php?action=gametracker&scoreboardTemplate=1&eventId=2384&active_tab=playbyplay&template=1)

Look now at the first drive. While watching the video, follow along with me with the sequences:

1. Play starts
2. Ball is snapped to Bortles
3. Pass directed to M. Lee (saying *nothing* about the **result**)
4. Q. Rollins on the defence
5. Incomplete for Bortles
6. Run by J. Thomas for 9 yards
7. Intercepted by J. Thomas (this is where this analysis falls apart. I believe #7 is the interception and #6 is the run for 9 yards)
8. Tackled by Linder
9. Fumbled by Thomas
10. Stripped by Linder
11. Recovered by Hyde
12. Tackled by Robinson

Obviously, for a human being tying those events to the description "(12:53) (Shotgun) B.Bortles pass short right intended for M.Lee INTERCEPTED by J.Thomas (Q.Rollins) at JAC 41. J.Thomas to JAC 32 for 9 yards (B.Linder). FUMBLES (B.Linder), recovered by GB-M.Hyde at JAC 29. M.Hyde to JAC 29 for no gain (D.Robinson)." wouldn't be all that hard, but it might be for a computer. 

By my eye, the way to do it might be to parse that string for instances and plays separately, because it appears instances always have 0 yards:

Instances:
0. Play starts
1. Bortles shotgun
2. Bortles pass
3. Defended by Rollins
4. Intercepted by Thomas
5. Fumbled by Thomas
6. Stripped by Linder
7. Recovered by Hyde
8. Tackled by Robinson

Plays:
1. Pass for 10 yards (short right)
2. Intercepted for 9 yards

We're missing one in those lists: incomplete by Bortles. I'd imagine you take that for granted in an INT. 

Interesting to look at and it's well put together. And, you can make a lot of assumptions where plays start and end thanks to the hash marks. 

Unfortunately, this doesn't hold entirely true for everything. For instance, drive 7. Those first couple of plays make sense to my eyes and brain but they'd be difficult to program. 